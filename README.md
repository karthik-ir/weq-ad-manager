### WeQ Advertisement Manager 

The application is build using 

* Java 8
* Spring-boot
* Maven

Install `maven` and `Java 8` as prerequisites. 
Build the application using `mvn clean package` this this should run the test cases and build the jar.
Build the docker image using the command `docker build -t weq:latest .`
Run the docker container using the command `docker run -p 8080:8080 weq:latest`


This is the high level representation fo the implementation. 
![Block Diagram](weq.png)

The service layer is separated to two parts 
1. Command
2. Query
 
This is  with the assumption that we expect a lot of frequent changes to the query part with some complex statistic requirement. And to hold the "Open-Closed principle" having this as a separate part would help in faster evolution of the system. 

The choice of Database is the RDBMS, since we are dealing with known set of attributes, schema on write is a better selection. And also since we would be writing more custom queries for statistical reporting it provides flexibility to tweak on it for performance. 

Also moving from one DB to another should not be much of a hassle because of the ORM choice(Hibernate). 


NOTE: 
1. Delivery API expects the Advertisement object to be present, therefore please run the /ads/advertisement POST request with body like `{"id":1234}`

2. In the `statistics` api please replace `+` with `%2b` since there are some encoding issues. 

3. With interest in time, the corner cases are not handled. 
	Like Null pointers if the ID is not passed or the parent ID is not passed. 
	The test cases are functional and not so elaborate enough. 
	This can be handled over time elaboration. 
	
	
Also Find below the ER diagram:

![ER Diagram](er-weq.png)


