package com.weq.admanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WeqApplication {

	public static void main(String[] args) {
		SpringApplication.run(WeqApplication.class, args);
	}
}
