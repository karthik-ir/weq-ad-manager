/**
 * 
 */
package com.weq.admanager.db.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author karthik
 *
 */
@Entity
@Table(name = "advertisement")
public class Advertisement {

	@Id
	private Long id;

	@OneToMany(mappedBy = "advertisement",fetch=FetchType.EAGER)
	private List<Delivery> deliveries;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Delivery> getDeliveries() {
		return deliveries;
	}

	public void setDeliveries(List<Delivery> delivery) {
		this.deliveries = delivery;
	}

}
