/**
 * 
 */
package com.weq.admanager.db.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author karthik
 *
 */
@Entity
@Table(name = "click")
public class Click {

	@Id
	private String id;

	@Column(name = "click_time")
	private Date time;

	@OneToMany(mappedBy = "click", fetch = FetchType.EAGER)
	private List<Install> installs;

	@ManyToOne
	@JoinColumn(name = "delivery_id")
	private Delivery delivery;

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public List<Install> getInstalls() {
		return installs;
	}

	public void setInstalls(List<Install> installs) {
		this.installs = installs;
	}

	public Delivery getDelivery() {
		return delivery;
	}

	public void setDelivery(Delivery delivery) {
		this.delivery = delivery;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
