/**
 * 
 */
package com.weq.admanager.db.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


/**
 * @author karthik
 *
 */

@Entity
@Table(name = "install")
public class Install {

	@Id
	private String id;

	@Column(name = "install_time")
	private Date time;

	@ManyToOne
	@JoinColumn(name="click_id")
	private Click click;

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Click getClick() {
		return click;
	}

	public void setClick(Click click) {
		this.click = click;
	}

}
