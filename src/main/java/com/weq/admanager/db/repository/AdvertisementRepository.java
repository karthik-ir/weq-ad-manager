/**
 * 
 */
package com.weq.admanager.db.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.weq.admanager.db.model.Advertisement;

/**
 * @author karthik
 *
 */
@Repository
public interface AdvertisementRepository extends JpaRepository<Advertisement, Long> {
}
