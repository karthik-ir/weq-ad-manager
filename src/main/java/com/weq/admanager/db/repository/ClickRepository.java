package com.weq.admanager.db.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.weq.admanager.db.model.Click;

/**
 * @author karthik
 *
 */
public interface ClickRepository extends JpaRepository<Click, String> {

}
