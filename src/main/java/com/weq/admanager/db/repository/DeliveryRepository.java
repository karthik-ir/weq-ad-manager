package com.weq.admanager.db.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.weq.admanager.db.model.Delivery;

/**
 * @author karthik
 *
 */
public interface DeliveryRepository extends JpaRepository<Delivery, String> {

}
