package com.weq.admanager.db.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.weq.admanager.db.model.Install;

/**
 * @author karthik
 *
 */
public interface InstallRepository extends JpaRepository<Install, String> {

}
