/**
 * 
 */
package com.weq.admanager.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.weq.admanager.rest.model.AdvertisementWebModel;
import com.weq.admanager.rest.model.ClickWebModel;
import com.weq.admanager.rest.model.DeliveryWebModel;
import com.weq.admanager.rest.model.InstallWebModel;
import com.weq.admanager.rest.model.SearchParams;
import com.weq.admanager.rest.model.StatisticResponse;
import com.weq.admanager.service.AdCommandService;
import com.weq.admanager.service.AdQueryService;
import com.weq.admanager.service.exception.ParentElementNotFoundException;

/**
 * @author karthik
 *
 */
@RestController
@RequestMapping("/ads")
public class AdController {

	@Autowired
	private AdCommandService adCommandService;

	@Autowired
	private AdQueryService adQueryService;

	@RequestMapping(value = "/advertisement", method = RequestMethod.POST, consumes = "application/json")
	private ResponseEntity createAdvertisement(@RequestBody AdvertisementWebModel advertisementWebModel) {
		Boolean createAd = adCommandService.createAd(advertisementWebModel);
		if (createAd)
			return new ResponseEntity(HttpStatus.OK);
		else
			return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@RequestMapping(value = "/delivery", method = RequestMethod.POST, consumes = "application/json")
	private ResponseEntity createDelivery(@RequestBody DeliveryWebModel deliveryWebModel)
			throws ParentElementNotFoundException {
		Boolean createDelivery = adCommandService.createDelivery(deliveryWebModel.getAdvertisementId(),
				deliveryWebModel);

		if (createDelivery)
			return new ResponseEntity(HttpStatus.OK);
		else
			return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@RequestMapping(value = "/click", method = RequestMethod.POST, consumes = "application/json")
	private ResponseEntity createClick(@RequestBody ClickWebModel clickWebModel) throws ParentElementNotFoundException {
		Boolean createClick = adCommandService.createClick(clickWebModel.getDeliveryId(), clickWebModel);

		if (createClick)
			return new ResponseEntity(HttpStatus.OK);
		else
			return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@RequestMapping(value = "/install", method = RequestMethod.POST, consumes = "application/json")
	private ResponseEntity createInstall(@RequestBody InstallWebModel installWebModel)
			throws ParentElementNotFoundException {
		Boolean createInstall = adCommandService.createInstall(installWebModel.getClickId(), installWebModel);

		if (createInstall)
			return new ResponseEntity(HttpStatus.OK);
		else
			return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@RequestMapping(value = "/statistics", method = RequestMethod.GET, produces = "application/json")
	private ResponseEntity<StatisticResponse> statistics( SearchParams searchParams) {
		StatisticResponse statistics = adQueryService.statistics(searchParams);
		return new ResponseEntity<StatisticResponse>(statistics, HttpStatus.OK);
	}
}
