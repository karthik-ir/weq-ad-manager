/**
 * 
 */
package com.weq.admanager.rest.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author karthik
 *
 */
public class AdvertisementWebModel {

	@JsonProperty(required=true)
	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
