/**
 * 
 */
package com.weq.admanager.rest.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.weq.admanager.rest.CustomDateWithNanoSecondsDeserializer;

/**
 * @author karthik
 *
 */
public class ClickWebModel {

	@JsonProperty(required = true, value = "clickId")
	private String id;
	@JsonProperty(required = true)
	private String deliveryId;
	@JsonDeserialize(using = CustomDateWithNanoSecondsDeserializer.class)
	private Date time;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public String getDeliveryId() {
		return deliveryId;
	}

	public void setDeliveryId(String deliveryId) {
		this.deliveryId = deliveryId;
	}
}
