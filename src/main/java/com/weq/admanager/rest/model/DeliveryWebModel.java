/**
 * 
 */
package com.weq.admanager.rest.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.weq.admanager.rest.CustomDateWithNanoSecondsDeserializer;

/**
 * @author karthik
 *
 */
public class DeliveryWebModel {

	@JsonProperty(required = true, value = "deliveryId")
	private String id;
	@JsonProperty(required = true)
	private Long advertisementId;
	@JsonDeserialize(using = CustomDateWithNanoSecondsDeserializer.class)
	private Date time;
	private String browser;
	private String os;
	private String site;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public String getBrowser() {
		return browser;
	}

	public void setBrowser(String browser) {
		this.browser = browser;
	}

	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}

	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	public Long getAdvertisementId() {
		return advertisementId;
	}

	public void setAdvertisementId(Long advertisementId) {
		this.advertisementId = advertisementId;
	}

}
