/**
 * 
 */
package com.weq.admanager.rest.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.weq.admanager.rest.CustomDateWithNanoSecondsDeserializer;

/**
 * @author karthik
 *
 */
public class InstallWebModel {

	@JsonProperty(required = true, value = "installId")
	private String id;
	@JsonProperty(required = true)
	private String clickId;
	@JsonDeserialize(using = CustomDateWithNanoSecondsDeserializer.class)
	private Date time;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public String getClickId() {
		return clickId;
	}

	public void setClickId(String clickId) {
		this.clickId = clickId;
	}

}
