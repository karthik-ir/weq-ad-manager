/**
 * 
 */
package com.weq.admanager.rest.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;

/**
 * @author karthik
 *
 */
public class SearchParams {

	@DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssZ")
	private Date start;
	@DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ssZ")
	private Date end;
	private List<String> group_by = new ArrayList<>();

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}

	public List<String> getGroup_by() {
		return group_by;
	}

	public void setGroup_by(List<String> group_by) {
		this.group_by = group_by;
	}

}
