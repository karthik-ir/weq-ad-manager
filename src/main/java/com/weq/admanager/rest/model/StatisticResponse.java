/**
 * 
 */
package com.weq.admanager.rest.model;

import java.util.List;
import java.util.Map;

/**
 * @author karthik
 *
 */
public class StatisticResponse {

	private Map<String, Object> interval;

	private List<Object> data;
	private Map<String, Object> stats;

	public Map<String, Object> getInterval() {
		return interval;
	}

	public void setInterval(Map<String, Object> interval) {
		this.interval = interval;
	}

	public Map<String, Object> getStats() {
		return stats;
	}

	public void setStats(Map<String, Object> stats) {
		this.stats = stats;
	}

	public List<Object> getData() {
		return data;
	}

	public void setData(List<Object> data) {
		this.data = data;
	}

}
