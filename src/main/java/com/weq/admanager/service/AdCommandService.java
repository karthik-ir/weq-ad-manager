/**
 * 
 */
package com.weq.admanager.service;

import org.springframework.stereotype.Service;

import com.weq.admanager.rest.model.AdvertisementWebModel;
import com.weq.admanager.rest.model.ClickWebModel;
import com.weq.admanager.rest.model.DeliveryWebModel;
import com.weq.admanager.rest.model.InstallWebModel;
import com.weq.admanager.service.exception.ParentElementNotFoundException;

/**
 * @author karthik
 *
 */
@Service
public interface AdCommandService {

	public Boolean createAd(AdvertisementWebModel ad);

	public Boolean createDelivery(Long adId, DeliveryWebModel delivery) throws ParentElementNotFoundException;

	public Boolean createClick(String deliveryId, ClickWebModel click) throws ParentElementNotFoundException;

	public Boolean createInstall(String clickId, InstallWebModel install) throws ParentElementNotFoundException;

}
