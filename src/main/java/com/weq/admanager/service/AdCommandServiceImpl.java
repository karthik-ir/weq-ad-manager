/**
 * 
 */
package com.weq.admanager.service;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.weq.admanager.db.model.Advertisement;
import com.weq.admanager.db.model.Click;
import com.weq.admanager.db.model.Delivery;
import com.weq.admanager.db.model.Install;
import com.weq.admanager.db.repository.AdvertisementRepository;
import com.weq.admanager.db.repository.ClickRepository;
import com.weq.admanager.db.repository.DeliveryRepository;
import com.weq.admanager.db.repository.InstallRepository;
import com.weq.admanager.rest.model.AdvertisementWebModel;
import com.weq.admanager.rest.model.ClickWebModel;
import com.weq.admanager.rest.model.DeliveryWebModel;
import com.weq.admanager.rest.model.InstallWebModel;
import com.weq.admanager.service.exception.ParentElementNotFoundException;

/**
 * @author karthik
 *
 */
@Component
public class AdCommandServiceImpl implements AdCommandService {

	@Autowired
	private AdvertisementRepository adRepo;

	@Autowired
	private DeliveryRepository deliveryRepo;

	@Autowired
	private ClickRepository clickRepo;

	@Autowired
	private InstallRepository installRepo;

	@Override
	public Boolean createAd(AdvertisementWebModel ad) {
		Advertisement advertisement = new Advertisement();
		BeanUtils.copyProperties(ad, advertisement);
		Advertisement save = adRepo.save(advertisement);
		if (save != null)
			return true;
		return false;
	}

	@Override
	@Transactional
	public Boolean createDelivery(Long adId, DeliveryWebModel delivery) throws ParentElementNotFoundException {
		if (!adRepo.exists(adId)) {
			throw new ParentElementNotFoundException();
		}

		Advertisement advertisement = adRepo.findOne(adId);

		Delivery delivery2 = new Delivery();
		BeanUtils.copyProperties(delivery, delivery2);
		delivery2.setAdvertisement(advertisement);

		Delivery save = deliveryRepo.save(delivery2);

		if (save != null)
			return true;

		return false;

	}

	@Override
	@Transactional
	public Boolean createClick(String deliveryId, ClickWebModel clickWebModel) throws ParentElementNotFoundException {
		if (!deliveryRepo.exists(deliveryId)) {
			throw new ParentElementNotFoundException();
		}

		Click click = new Click();
		BeanUtils.copyProperties(clickWebModel, click);
		click.setDelivery(deliveryRepo.findOne(deliveryId));

		click = clickRepo.save(click);
		if (click != null)
			return true;

		return false;
	}

	@Override
	@Transactional
	public Boolean createInstall(String clickId, InstallWebModel installWebModel)
			throws ParentElementNotFoundException {
		if (!clickRepo.exists(clickId)) {
			throw new ParentElementNotFoundException();
		}
		

		Install install = new Install();
		BeanUtils.copyProperties(installWebModel, install);
		install.setClick(clickRepo.findOne(clickId));

		install = installRepo.save(install);

		if (install != null)
			return true;
		return false;
	}

}
