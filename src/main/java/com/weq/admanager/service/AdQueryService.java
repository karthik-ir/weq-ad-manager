/**
 * 
 */
package com.weq.admanager.service;

import org.springframework.stereotype.Service;

import com.weq.admanager.rest.model.SearchParams;
import com.weq.admanager.rest.model.StatisticResponse;

/**
 * @author karthik
 *
 */
@Service
public interface AdQueryService {

	StatisticResponse statistics(SearchParams searchParams);
}
