/**
 * 
 */
package com.weq.admanager.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.weq.admanager.rest.model.SearchParams;
import com.weq.admanager.rest.model.StatisticResponse;

/**
 * @author karthik
 *
 */
@Component
public class AdQueryServiceImpl implements AdQueryService {

	@Autowired
	EntityManager em;

	@Override
	public StatisticResponse statistics(SearchParams searchParams) {
		Query query = buildQuery(searchParams);
		List<Object> stats = query.getResultList();

		StatisticResponse statisticResponse = new StatisticResponse();

		Map<String, Object> interval = new HashMap<>();
		interval.put("start", searchParams.getStart());
		interval.put("end", searchParams.getEnd());

		statisticResponse.setInterval(interval);

		List<Object> collect = null;
		if (searchParams.getGroup_by() != null && searchParams.getGroup_by().size() != 0) {
			collect = stats.stream().map(stat -> {
				Object[] sts = (Object[]) stat;
				Map<String, Object> statso = new HashMap<>();
				statso.put("deliveries", sts[0 + searchParams.getGroup_by().size()]);
				statso.put("clicks", sts[1 + searchParams.getGroup_by().size()]);
				statso.put("installs", sts[2 + searchParams.getGroup_by().size()]);

				Map<String, Object> fields = new HashMap<>();

				if (searchParams.getGroup_by() != null && searchParams.getGroup_by().size() != 0) {
					int i = 0;
					for (String group : searchParams.getGroup_by()) {
						fields.put(group, sts[i]);
						i++;
					}
				}

				Map<String, Object> data = new HashMap<>();
				data.put("fields", fields);
				data.put("stats", statso);

				return data;
			}).collect(Collectors.toList());
			statisticResponse.setData(collect);
		} else {
			Object[] sts = (Object[]) stats.get(0);
			Map<String, Object> statso = new HashMap<>();
			statso.put("deliveries", sts[0 + searchParams.getGroup_by().size()]);
			statso.put("clicks", sts[1 + searchParams.getGroup_by().size()]);
			statso.put("installs", sts[2 + searchParams.getGroup_by().size()]);
			statisticResponse.setStats(statso);
		}

		return statisticResponse;
	}

	Query buildQuery(SearchParams searchParams) {
		Query query;
		String queryString = "select count(distinct d) as delivery,count(distinct c) as click, count(distinct i) as install from Delivery d left join d.clicks c left join c.installs i where d.time>=?0 and d.time<=?1";

		if (searchParams.getGroup_by() != null && searchParams.getGroup_by().size() != 0) {
			queryString = queryString + " group by " + String.join(",", searchParams.getGroup_by().stream().map(val -> {
				return "d." + val;
			}).collect(Collectors.toList()));

			queryString = queryString.replace("select ",
					"select " + String.join(",", searchParams.getGroup_by().stream().map(val -> {
						return "d." + val;
					}).collect(Collectors.toList())) + ",");

			query = em.createQuery(queryString);
			query.setParameter(0, searchParams.getStart());
			query.setParameter(1, searchParams.getEnd());
		} else {
			query = em.createQuery(queryString);
			query.setParameter(0, searchParams.getStart());
			query.setParameter(1, searchParams.getEnd());
		}
		return query;
	}

}
