
CREATE TABLE IF NOT EXISTS advertisement (
    id bigint PRIMARY KEY NOT NULL
);

CREATE TABLE IF NOT EXISTS delivery (
    id VARCHAR(64) primary key not null,
    advertisement_id bigint,
    browser VARCHAR(30),
    os VARCHAR(30),
    site VARCHAR(100),
    delivery_time DATETIME,
    FOREIGN KEY (advertisement_id)
        REFERENCES advertisement (id)
);

CREATE TABLE IF NOT EXISTS click (
    id VARCHAR(64) primary key not null,
    delivery_id VARCHAR(64),
    click_time DATETIME,
    FOREIGN KEY (delivery_id)
        REFERENCES delivery (id)
);


CREATE TABLE IF NOT EXISTS install (
    id VARCHAR(64) PRIMARY KEY NOT NULL,
    click_id VARCHAR(64),
    install_time DATETIME,
    FOREIGN KEY (click_id)
        REFERENCES click (id)
)
