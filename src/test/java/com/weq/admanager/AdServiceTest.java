/**
 * 
 */
package com.weq.admanager;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.weq.admanager.db.repository.AdvertisementRepository;
import com.weq.admanager.db.repository.ClickRepository;
import com.weq.admanager.db.repository.DeliveryRepository;
import com.weq.admanager.rest.model.AdvertisementWebModel;
import com.weq.admanager.rest.model.ClickWebModel;
import com.weq.admanager.rest.model.DeliveryWebModel;
import com.weq.admanager.rest.model.InstallWebModel;
import com.weq.admanager.rest.model.SearchParams;
import com.weq.admanager.rest.model.StatisticResponse;
import com.weq.admanager.service.AdCommandService;
import com.weq.admanager.service.AdQueryService;
import com.weq.admanager.service.exception.ParentElementNotFoundException;

/**
 * @author karthik
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest

public class AdServiceTest {

	@Autowired
	AdCommandService adService;

	@Autowired
	private AdvertisementRepository adRepo;

	@Autowired
	private DeliveryRepository deliveryRepository;

	@Autowired
	private ClickRepository clickRepository;

	@Autowired
	private AdQueryService adQueryService;

	public void testAdCreation(Long id) {
		AdvertisementWebModel advertisement = new AdvertisementWebModel();

		advertisement.setId(id);
		Boolean createAd = adService.createAd(advertisement);
		assertTrue(createAd);

		assertTrue(adRepo.exists(id));
		assertEquals(adRepo.findOne(id).getDeliveries().size(), 0);
	}

	@Test
	public void testDeliveryCreationWithValidAdID() throws ParentElementNotFoundException {

		long AdvertisementId = 4321L;
		testAdCreation(AdvertisementId);

		DeliveryWebModel delivery = new DeliveryWebModel();
		delivery.setBrowser("Chrome");
		delivery.setOs("ios");
		delivery.setSite("http://super-dooper-news.com");
		String deliveryId = "244cf0db-ba28-4c5f-8c9c-2bf11ee42988";
		delivery.setId(deliveryId);
		delivery.setTime(new Date());
		Boolean createDelivery = adService.createDelivery(AdvertisementId, delivery);
		assertTrue(createDelivery);

		delivery = new DeliveryWebModel();
		delivery.setBrowser("Chrome");
		delivery.setOs("ios");
		delivery.setSite("http://super-dooper-news.com");
		deliveryId = "244cf0db-ba28-4c5f-8c9c-2bf11ee42989";
		delivery.setId(deliveryId);
		delivery.setTime(new Date());
		createDelivery = adService.createDelivery(AdvertisementId, delivery);
		assertTrue(createDelivery);

		delivery = new DeliveryWebModel();
		delivery.setBrowser("Chrome");
		delivery.setOs("android");
		delivery.setSite("http://super-dooper-news.com");
		deliveryId = "244cf0db-ba28-4c5f-8c9c-2bf11ee42989";
		delivery.setId(deliveryId);
		delivery.setTime(new Date());
		createDelivery = adService.createDelivery(AdvertisementId, delivery);
		assertTrue(createDelivery);

		com.weq.admanager.db.model.Advertisement findOne = adRepo.findOne(AdvertisementId);
		List<com.weq.admanager.db.model.Delivery> delivieries = findOne.getDeliveries();

		assertEquals(delivieries.size(), 2);
		assertEquals(delivieries.get(0).getBrowser(), "Chrome");
		assertEquals(delivieries.get(0).getOs(), "ios");
		assertEquals(delivieries.get(1).getOs(), "android");

		ClickWebModel clickWebModel = new ClickWebModel();
		clickWebModel.setId("fff54b83-49ff-476f-8bfb-2ec22b252c32");
		clickWebModel.setTime(new Date());

		Boolean createClick = adService.createClick("244cf0db-ba28-4c5f-8c9c-2bf11ee42989", clickWebModel);
		assertTrue(createClick);

		assertEquals(1, deliveryRepository.findOne("244cf0db-ba28-4c5f-8c9c-2bf11ee42989").getClicks().size());

		InstallWebModel installWebModel = new InstallWebModel();
		installWebModel.setId("144cf0db-ba28-4c5f-8c9c-2bf11ee42988");
		installWebModel.setTime(new Date());
		Boolean createInstall = adService.createInstall("fff54b83-49ff-476f-8bfb-2ec22b252c32", installWebModel);
		assertTrue(createInstall);

		assertEquals(1, clickRepository.findOne("fff54b83-49ff-476f-8bfb-2ec22b252c32").getInstalls().size());
		assertEquals("144cf0db-ba28-4c5f-8c9c-2bf11ee42988",
				clickRepository.findOne("fff54b83-49ff-476f-8bfb-2ec22b252c32").getInstalls().get(0).getId());

		SearchParams searchParams = new SearchParams();
		searchParams.setStart(new Date(System.currentTimeMillis() - 1000L * 60L * 60L * 24L));
		searchParams.setEnd(new Date());
		List<String> groupby = new ArrayList<>();
		groupby.add("browser");
		groupby.add("os");
		searchParams.setGroup_by(groupby);
		StatisticResponse statistics = adQueryService.statistics(searchParams);
		assertEquals(statistics.getData().size(), 2);

		searchParams.setGroup_by(new ArrayList<>());
		statistics = adQueryService.statistics(searchParams);
		assertEquals(statistics.getData(), null);
		assertEquals(statistics.getStats().get("deliveries"), 2L);
		assertEquals(statistics.getStats().get("installs"), 1L);

	}

	@Test(expected = ParentElementNotFoundException.class)
	public void testDeliveryCreationWithInValidAdID() throws ParentElementNotFoundException {
		DeliveryWebModel delivery = new DeliveryWebModel();
		delivery.setBrowser("Chrome");
		delivery.setOs("ios");
		delivery.setSite("http://super-dooper-news.com");
		String deliveryId = "244cf0db-ba28-4c5f-8c9c-2bf11ee42999";
		delivery.setId(deliveryId);
		delivery.setTime(new Date());
		Boolean createDelivery = adService.createDelivery(4331L, delivery);
		assertFalse(createDelivery);

		com.weq.admanager.db.model.Delivery adObj = deliveryRepository.findOne(deliveryId);
		assertNull(adObj);
	}

	@Test(expected = ParentElementNotFoundException.class)
	public void testClickCreationWithInvalidDeliveryId() throws ParentElementNotFoundException {
		adService.createClick("invlid", new ClickWebModel());
	}

	@Test(expected = ParentElementNotFoundException.class)
	public void testInstallCreationWithInvalidClickID() throws ParentElementNotFoundException {
		adService.createInstall("invalid", new InstallWebModel());
	}
}
